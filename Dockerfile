FROM node:latest

MAINTAINER cybernemo

RUN \
 git clone https://github.com/xibyte/jsketcher.git && \
 cd jsketcher && \
 npm install

WORKDIR jsketcher

CMD ["npm", "start"]

EXPOSE 3000
